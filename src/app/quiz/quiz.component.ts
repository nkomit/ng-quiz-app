import { Component, OnInit } from '@angular/core';

export interface Option {
  text: string;
  comment: string;
  link?: string;
  fragment?: string,
  clickable?: string;
  correct: boolean;
  chosen: boolean;
}

export interface Question {
  story: string;
  index: number;
  answered: boolean;
  options: Option[];
}

export const QUESTIONS: Question[] = [
  {
    story: 'Посетителю компьютерного клуба в Улан-Удэ стало плохо. Однако, когда скорая доставила его в больницу, он внезапно отказался от госпитализации. В чем подвох?',
    index: 1,
    answered: false,
    options: [
      {
        text: 'Он жил рядом и так сэкономил на такси.',
        comment: 'Не угадали!',
        correct: false,
        chosen: false
      },
      {
        text: 'Он поспорил с друзьями на крупную сумму, что прокатится в скорой.',
        comment: 'Не угадали!',
        correct: false,
        chosen: false
      },
      {
        text: 'Он обворовал скорую.',
        comment: 'Верно! Вместо лечения геймер решил украсть из скорой сумку. Правда, вскоре открыл ее и разочаровался, увидев кардиограф. Несмотря на то что кардиограф стоит 85 000 рублей, молодой человек решил выкинуть его.',
        correct: true,
        chosen: false
      },
      {
        text: 'Ему нужно было увидеть скорую изнутри, чтобы пройти уровень в игре.',
        comment: 'Не угадали!',
        correct: false,
        chosen: false
      },
    ]
  },
  {
    story: 'Полицейские из Мурманской области заплатили мужчине, чтобы он выловил семгу. Что было дальше?',
    index: 2,
    answered: false,
    options: [
      {
        text: 'Они отравились и отдали рыбака под суд.',
        comment: 'Никак нет!',
        correct: false,
        chosen: false
      },
      {
        text: 'Втроем они организовали подпольный рынок красной рыбы.',
        comment: 'Никак нет!',
        correct: false,
        chosen: false
      },
      {
        text: 'Внутри семги нашли золотые монеты, из-за которых началась драка.',
        comment: 'Никак нет!',
        correct: false,
        chosen: false
      },
      {
        text: 'Они арестовали его за незаконный вылов рыбы.',
        comment: 'Так точно! Полицейские сняли рыбалку на камеру, задержали нарушителя и возбудили уголовное дело ради премии.',
        correct: true,
        chosen: false
      },
    ]
  },
  {
    story: 'В Азове мужчина с пистолетом похитил эвакуатор. Какие у него были дальнейшие планы на вечер?',
    index: 3,
    answered: false,
    options: [
      {
        text: 'Переставить машину любовника жены.',
        comment: 'Мимо!',
        correct: false,
        chosen: false
      },
      {
        text: 'Эвакуировать холодильник с мороженым.',
        comment: 'Мимо!',
        correct: false,
        chosen: false
      },
      {
        text: 'Поиграть в «Стопхам» и убрать неправильно припаркованные авто.',
        comment: 'Мимо!',
        correct: false,
        chosen: false
      },
      {
        text: 'Выкрасть на эвакуаторе свою же машину со штрафстоянки.',
        comment: 'Да! До этого нетрезвый водитель попал в ДТП с двумя грузовиками и таким способом хотел вернуть авто со стоянки.',
        correct: true,
        chosen: false
      },
    ]
  },
  {
    story: 'Житель Краснодарского края гулял с собакой, увидел незапертую машину с ключами в замке зажигания и угнал автомобиль. Что толкнуло его на преступление?',
    index: 4,
    answered: false,
    options: [
      {
        text: 'Шел дождь, и он хотел спасти собаку от непогоды.',
        comment: 'Именно. Владелец собаки был пьян и хотел переждать ливень, но не удержался и прокатился. У него не было прав, поэтому он оставил машину разбитой у первого дерева и пошел домой. Полиция легко нашла угонщика.',
        correct: true,
        chosen: false
      },
      {
        text: 'Он узнал, что у него родился сын.',
        comment: 'Все было не так.',
        correct: false,
        chosen: false
      },
      {
        text: 'Он решил, что это его машина, которую угнали в прошлом месяце.',
        comment: 'Дело не всегда в деньгах.',
        correct: false,
        chosen: false
      },
      {
        text: 'Внутри было много ящиков с алкоголем.',
        comment: 'Все было не так.',
        correct: false,
        chosen: false
      },
    ]
  },
  {
    story: 'Коллектор из Хабаровска очень хотел получить премию за возврат кредита должником, но выбрал для этого специфический метод. Какой?',
    index: 5,
    answered: false,
    options: [
      {
        text: 'Угнать машину неплательщика.',
        comment: 'А лучше бы угнал!',
        correct: false,
        chosen: false
      },
      {
        text: 'Погасить кредит своими средствами.',
        comment: 'Увы.',
        correct: false,
        chosen: false
      },
      {
        text: 'Выложить в интернет порнографический коллаж с участием ребенка должника.',
        comment: 'Что этим хотел сказать автор, непонятно. Сейчас бывший сотрудник микрофинансовой организации ждёт приговора за изготовление и распространение порнографии.',
        correct: true,
        chosen: false
      },
      {
        text: 'Похитить и пытать заемщика.',
        comment: 'До этого не дошло.',
        correct: false,
        chosen: false
      },
    ]
  },
  {
    story: 'В 2019 году россияне часто пытались добиться своих целей шантажом. Один калининградец украл у бывшей жены паспорт и страховой полис. Что он за них требовал?',
    index: 6,
    answered: false,
    options: [
      {
        text: 'Вернуться к нему.',
        comment: 'Нет, он требовал оплатить его долги за коммунальные услуги.',
        correct: false,
        chosen: false
      },
      {
        text: 'Готовить ему еду.',
        comment: 'Нет, он требовал оплатить его долги за коммунальные услуги.',
        correct: false,
        chosen: false
      },
      {
        text: 'Оплатить его коммуналку.',
        comment: 'Да! Сначала бывший муж просто просил деньги, но когда услышал отказ, то перешел к решительным методам. Теперь на него завели уголовное дело за похищение документов с целью наживы.',
        correct: true,
        chosen: false
      },
      {
        text: 'Вернуть помолвочное кольцо.',
        comment: 'Нет, он требовал оплатить его долги за коммунальные услуги.',
        correct: false,
        chosen: false
      },
    ]
  },
  {
    story: 'Житель Морозовска взломал ворота частного дома, вскрыл гараж и угнал машину. Далеко уехать не получилось: в ближайшем лесу авто заглохло. Как тогда решил заработать угонщик?',
    index: 7,
    answered: false,
    options: [
      {
        text: 'Вернулся в дом и потребовал вознаграждение за информацию о местонахождении машины.',
        comment: 'Дончанин решил довольствоваться малым.',
        correct: false,
        chosen: false
      },
      {
        text: 'Достал из багажника бензопилу и вырезал аккумулятор!',
        comment: 'Да, техасский маньяк гордился бы. Дончанин спрятал выпиленный аккумулятор у друга, но это не помогло — его нашли и обвиняют по двум статьям.',
        correct: true,
        chosen: false
      },
      {
        text: 'Выставил машину на продажу в таком состоянии.',
        comment: 'Нет, он достал из багажника бензопилу и выпилил аккумулятор.',
        correct: false,
        chosen: false
      },
      {
        text: 'Открыл аттракцион «Ударь по машине кувалдой за 100 рублей!».',
        comment: 'Нет, он достал из багажника бензопилу и выпилил аккумулятор.',
        correct: false,
        chosen: false
      },
    ]
  },
  {
    story: 'Житель Омска нашел на остановке пять тысяч рублей «Банка приколов». Как он поступил с неожиданным богатством?',
    index: 8,
    answered: false,
    options: [
      {
        text: 'Купил билеты в парк аттракционов и раздавал их прохожим.',
        comment: 'О да! Омич купил 31 билет на карусели и раздал их родителям с детьми. Уже на выходе из парка его поджидали полицейские, которых вызвал кассир.',
        correct: true,
        chosen: false
      },
      {
        text: 'Купил лотерейные билеты и выиграл миллион.',
        comment: 'Ну нет.',
        correct: false,
        chosen: false
      },
      {
        text: 'Дал взятку.',
        comment: 'Ну нет.',
        correct: false,
        chosen: false
      },
      {
        text: 'Накупил товаров в магазине приколов.',
        comment: 'Ну нет.',
        correct: false,
        chosen: false
      },
    ]
  }
];


@Component({
  selector: 'app-quiz',
  templateUrl: './quiz.component.html',
  styleUrls: ['./quiz.component.scss']
})

export class QuizComponent implements OnInit {
  questions = QUESTIONS;
  question!: Question;
  score: number = 0;
  questIndex!: number;

  ngOnInit() {
    this.resetQuiz();
  }

  resetQuiz() {
    this.questIndex = 0;
    this.question = this.questions[this.questIndex];
    this.score = 0;
    for (let i of this.questions) {
      i.answered = false;
      for (let j of i.options) {
        j.chosen = false;
      }
    }
  }

  onAnswered() {
    this.question.answered = true;
  }

  onChecked(option: Option) {
    option.chosen = true;
    if (option.correct === true) {
      this.score++;
    }
  }

  nextQuestion(question: Question){
    this.questIndex++;
    question = this.questions[this.questIndex];
    this.question = question;
  }
}

import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { MatRadioModule } from '@angular/material/radio';
import { MatCardModule } from '@angular/material/card';

import { QuizComponent } from './quiz/quiz.component';

@NgModule({
  declarations: [
    QuizComponent,
  ],
  imports: [
    BrowserModule,
    MatRadioModule,
    MatCardModule
  ],
  bootstrap: [QuizComponent]
})
export class AppModule { }
